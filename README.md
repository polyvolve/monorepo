<p align="center">
    <br>
        <img src="https://polyvolve.gitlab.io/site/logo.svg" width="128"/>
    <br>
<p>

# Polyvolve

Polyvolve is a SaaS app to manage automated performance reviews in an organization. I put special emphasis on bottom-up reviews.

This is not a professional project. It was an attempt to build a SaaS template and prototype with some business relevance, primarily for education purposes.

The development is on hold. The current state is sufficient to show an idea of the template/prototype. Screenshots can be seen at the bottom.

## Repos

### api

The REST API backend for Polyvolve. This repo is private (for now). Runs on Heroku.

### app-admin

The administration web app which can be used for managing users and reviews. This repo is private (for now). Runs on Heroku to support the routing logic.

Can be visited at: https://polyvolve-admin.herokuapp.com

User: test@test.com

Pass: test

### app-consumer

The frontend for the actual user who will perform the reviews on the specified. The reviewing process goes as follows: Receive mail -> click on the personal link -> respond to the questions (called schema in app-admin).

Currently not testable because the mail delivery service is disabled (to prevent spam).

Runs on Gitlab Pages.

### site

A demo promotional page. The shown text (features and intro message) is only demo-worthy. Generally, creating a decent page on a budget has proven to be quite a challenge.

Runs on Gitlab Pages.

Can be visited at: https://polyvolve.gitlab.io/site

### ui

Contains shared components for app-admin and app-consumer. The consumers must be configured to transpile the library.

## Todo

- Documentation
- Tests
- Onboarding
- Test with other browsers
- Mobile optimization
- ...

## Tech stack

### Frontend

- React
- Typescript
- Next.js
- Redux
- SASS

### Backend

- Kotlin
- PostgreSQL
- JWT
- Spring Boot

### Hosting

- Heroku
- Gitlab Pages

# Screenshots

Note: these are just placeholder values.

<img src="https://polyvolve.gitlab.io/site/static/ReviewByClient.jpg" width="800"/>
<br>
<br>
<img src="https://polyvolve.gitlab.io/site/static/ReviewMasterConfig.jpg" width="800"/>
